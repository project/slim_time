Drupal Module: Slim Time
Author: Aaron Klump sourcecode@intheloftstudios.com

Summary

Provides a visually-lean time widget for use with text fields, e.g., when you need to collect something like “6:45pm”. It let’s users type the time, but provides subtle corrections and expansions.

You may also visit the project page on Drupal.org.

Installation

Download the Slim Time jQuery plugin from here: http://www.intheloftstudios.com/packages/jquery/jquery.slim_time/demo/index.html and install it in sites/all/libraries/slim_time
Install as usual, see http://drupal.org/node/70151 for further information.
Visit admin/reports/status and make sure the library is detected; if all is well it will read “Successfully loaded library from sites/all/libraries/slim_time”.
Configuration

Create or edit a textfield and select Slim time as the widget.
Configuration options can be found on the field edit page.
Visit the node edit page and try entering some time.
Contact

In the Loft Studios
Aaron Klump - Developer
PO Box 29294 Bellingham, WA 98228–1294
skype: intheloftstudios
d.o: aklump
http://www.InTheLoftStudios.com